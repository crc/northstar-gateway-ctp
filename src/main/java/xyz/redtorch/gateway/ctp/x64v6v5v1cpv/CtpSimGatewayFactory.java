package xyz.redtorch.gateway.ctp.x64v6v5v1cpv;

import org.dromara.northstar.common.event.FastEventEngine;
import org.dromara.northstar.common.model.GatewayDescription;
import org.dromara.northstar.gateway.Gateway;
import org.dromara.northstar.gateway.GatewayFactory;
import org.dromara.northstar.gateway.IMarketCenter;
import org.dromara.northstar.gateway.ctp.CtpDataServiceManager;
import org.dromara.northstar.gateway.ctp.CtpSimGatewaySettings;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson2.JSONObject;

public class CtpSimGatewayFactory implements GatewayFactory{

	private FastEventEngine fastEventEngine;
	private IMarketCenter mktCenter;
	private CtpDataServiceManager dataMgr;

	public CtpSimGatewayFactory(FastEventEngine fastEventEngine, IMarketCenter mktCenter, CtpDataServiceManager dataMgr) {
		this.fastEventEngine = fastEventEngine;
		this.mktCenter = mktCenter;
		this.dataMgr = dataMgr;
	}

	@Override
	public Gateway newInstance(GatewayDescription gatewayDescription) {
		CtpSimGatewaySettings settings = JSON.parseObject(JSON.toJSONString(gatewayDescription.getSettings()), CtpSimGatewaySettings.class);
		JSONObject json = dataMgr.getCtpMetaSettings(settings.getBrokerId());
		settings.setAppId(json.getString("appId"));
		settings.setAuthCode(json.getString("authCode"));
		settings.setMdPort(json.getString("mdPort"));
		settings.setTdPort(json.getString("tdPort"));
		return new CtpSimGatewayAdapter(fastEventEngine, gatewayDescription.toBuilder().settings(settings).build(), mktCenter);
	}

}
